"""
DCell+POX.  As simple a data center controller as possible.
"""

from pox.core import core
from pox.lib.util import dpidToStr
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import EventMixin

from dcell.mn import topos
from dcell.dctopo import DCellTopo

from util import buildTopo, getRouting
from time import sleep, time

log = core.getLogger()

# Number of bytes to send for packet_ins
MISS_SEND_LEN = 2000

# Borrowed from pox/forwarding/l2_multi
class Switch (EventMixin):
  def __init__ (self):
    self.connection = None
    self.ports = None
    self.dpid = None
    self._listeners = None

  def __repr__ (self):
    return dpidToStr(self.dpid)

  def disconnect (self):
    if self.connection is not None:
      log.debug("Disconnect %s" % (self.connection,))
      self.connection.removeListeners(self._listeners)
      self.connection = None
      self._listeners = None

  def connect (self, connection):
    if self.dpid is None:
      self.dpid = connection.dpid
    assert self.dpid == connection.dpid
    if self.ports is None:
      self.ports = connection.features.ports
    self.disconnect()
    #log.debug("Connect %s" % (connection,))
    self.connection = connection
    self._listeners = self.listenTo(connection)

  def send_packet_data(self, outport, data = None):
    msg = of.ofp_packet_out(in_port=of.OFPP_NONE, data = data)
    msg.actions.append(of.ofp_action_output(port = outport))
    self.connection.send(msg)

  def send_packet_bufid(self, outport, buffer_id = -1):
    msg = of.ofp_packet_out(in_port=of.OFPP_NONE)
    msg.actions.append(of.ofp_action_output(port = outport))
    msg.buffer_id = buffer_id
    self.connection.send(msg)

  def install(self, port, match, buf = -1):
    msg = of.ofp_flow_mod()
    msg.match = match
    msg.idle_timeout = 10
    msg.hard_timeout = 600 
    msg.actions.append(of.ofp_action_output(port = port))
    msg.buffer_id = buf
    self.connection.send(msg)

  def _handle_ConnectionDown (self, event):
    self.disconnect()
    pass


class DCellController(EventMixin):

  def __init__ (self, t, r):
    self.switches = {}  # Switches seen: [dpid] -> Switch
    self.t = t  # Master Topo object, passed in and never modified.
    self.r = r  # Master Routing object, passed in and reused.
    self.macTable = {}  # [mac] -> (dpid, port)
    self.failedLinks = [] # [sw1, sw2] switches connecting the failed link
    self.failureDetected = False

    # TODO: generalize all_switches_up to a more general state machine.
    self.all_switches_up = False  # Sequences event handling.
    self.listenTo(core.openflow, priority=0)
    self.populateMACTables(t)

  def populateMACTables(self, t):
    for c in range(1, t.n+2):
	for h in range(1, t.n+1):
	    host_mac = t.id_gen(t.LAYER_HOST, c, h).mac_str()
	    switch_dpid = t.id_gen(t.LAYER_SWITCH, c, h).get_id()
	    self.macTable[host_mac] = (switch_dpid, 1)
    

  def _raw_dpids(self, arr):
    "Convert a list of name strings (from Topo object) to numbers."
    return [self.t.id_gen(name = a).dpid for a in arr]

  def _install_path(self, event, out_dpid, final_out_port, packet):
    "Install entries on route between two switches."
    in_name = self.t.id_gen(dpid = event.dpid).name_str()
    out_name = self.t.id_gen(dpid = out_dpid).name_str()
    route = self.t.get_failover_route(in_name, out_name, self.failedLinks)
    log.info("route: %s" % route)
    match = of.ofp_match.from_packet(packet)
    for i, node in enumerate(route):
      node_dpid = self.t.id_gen(name = node).dpid
      if i < len(route) - 1:
        next_node = route[i + 1]
        out_port, next_in_port = self.t.port(node, next_node)
      else:
        out_port = final_out_port
      self.switches[node_dpid].install(out_port, match)

  def clearPaths(self):
      clear = of.ofp_flow_mod(match=of.ofp_match(),command=of.OFPFC_DELETE)
      for sw in self.switches.itervalues():
          sw.connection.send(clear)

  def _handle_PortStatus (self, event):
    #log.info("DCellController: _handle_PortStatus")
    if event.ofp.desc.state == of.OFPPS_LINK_DOWN:
      log.info("port#: %d of %s DOWN" % (event.ofp.desc.port_no, event.ofp.desc.name))
      linkPort = event.ofp.desc.name
      name, port = linkPort.split("-")
      if not self.failureDetected :
	  self.failureDetected = True
	  if (port == "eth3") :
              #Link failure
	      sleep (1)
	  else :
              #Server failure
              log.info("SERVER Failure ............")
	      sleep (5)
      if (port == "eth3") and not self.failedLinks :
         if name not in self.failedLinks:
            self.failedLinks.append(name)
	 remoteSw = self.t.get_remote_switch(name)
	 if remoteSw not in self.failedLinks:
	    self.failedLinks.append(remoteSw)
	 self.clearPaths()
      #print self.failedLinks
    elif event.ofp.desc.state == of.OFPPS_STP_LISTEN:
      log.info("port#: %d of %s UP" % (event.ofp.desc.port_no, event.ofp.desc.name))
      linkPort = event.ofp.desc.name
      name, port = linkPort.split("-")
      if name in self.failedLinks:
         self.failedLinks.remove(name)
      #print self.failedLinks
      if (len(self.failedLinks) == 0):
	  self.failureDetected = False
	  self.clearPaths()
    else:
      print "port#: %d of %s in state" % (event.ofp.desc.port_no, event.ofp.desc.name, event.ofp.desc.state)

  def _handle_PacketIn(self, event):
    #log.info("DCellController: _handle_PacketIn")
    #log.info("Parsing PacketIn.")

    if not self.all_switches_up:
      log.info("Saw PacketIn before all switches were up - ignoring.")
      return
    else:
      packet = event.parsed
      dpid = event.dpid
      #log.info("PacketIn: packet = %s" % packet)
      in_port = event.port
      t = self.t

      #log.info("packet: %s %s  %d  %d" % (packet.src, packet.dst, dpid, in_port))

      # Learn MAC address of the sender on every packet-in.
      #self.macTable[packet.src] = (dpid, in_port)
  
      #log.info("mactable: %s" % self.macTable)
      dstMacAddr = packet.dst.toStr()
      
      # Insert flow, deliver packet directly to destination.
      if dstMacAddr in self.macTable:
        out_dpid, out_port = self.macTable[dstMacAddr]
        self._install_path(event, out_dpid, out_port, packet)

        #log.info("sending to entry in mactable: %s %s" % (out_dpid, out_port))
        self.switches[out_dpid].send_packet_data(out_port, event.data)

      else:
        # log.info("Broadcasting")
        # Broadcast to every switch port of Host-Switch-Combo port except the input on the input switch.
	sourceId = t.id_gen(dpid = dpid)
	sourceName = sourceId.name_str() 
	if (sourceId.isHost() == 1) :
	    log.info("Didn't expect it %s " % sourceName)
	    return
        for host in range(1, t.n+1): #No need to go to the miniswitch
	    for cell in range(1, t.n+2):
		switchName = "0_" + str(cell) + "_" + str(host)
		if (switchName != sourceName):
		    destId = t.id_gen(name = switchName).get_id()
                    dstSwitch = self.switches[destId]
		    dstSwitch.send_packet_data(1, event.data)


  def _handle_ConnectionUp (self, event):
    #log.info("DCellController: _handle_ConnectionUp") 
    sw = self.switches.get(event.dpid)
    name_str = self.t.id_gen(dpid = event.dpid).name_str()
    #log.info("Saw switch come up: %s", name_str)
    if name_str not in self.t.switches():
      log.warn("Ignoring unknown switch %s" % name_str)
      return
    if sw is None:
      #log.info("Added fresh switch %s for dpid %d " % (name_str, event.dpid))
      sw = Switch()
      self.switches[event.dpid] = sw
      sw.connect(event.connection)
    else:
      log.info("Odd - already saw switch %s come up" % name_str)
      sw.connect(event.connection)
    sw.connection.send(of.ofp_set_config(miss_send_len=MISS_SEND_LEN))

    if len(self.switches) == len(self.t.switches()):
      log.info(" ------- Woo!  All switches up ------------------------")
      self.all_switches_up = True


def launch(topo = None, routing = None):
  """
  Args in format toponame,arg1,arg2,...
  """
  # Instantiate a topo object from the passed-in file.
  if not topo:
    raise Exception("please specify topo and args on cmd line")
  else:
    t = buildTopo(topo, topos)
    r = getRouting(routing, t)

  core.registerNew(DCellController, t, r)

  log.info("DCell-POX running with topo=%s." % topo)
