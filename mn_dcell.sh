#! /bin/bash
iperf=/usr/bin/iperf

echo " "
echo " "
echo " ******* cleanup any previous tests ********************* "
echo " "
echo " "

# clean up previous runs
sudo killall -9 $iperf
sudo mn -c

exptid=`date +%b%d-%H-%M`

# setup test parameters -----------------------
bandwidth=100         # 100Mbps
directoryName=$exptid # output directory
numHosts=4            # Number of host per DCell

echo " "
echo " "
echo " ****** Starting Experiment  ******************************"
echo " "
echo " ---- output being captured in file run_$exptid.log -------"
echo " "
echo " check $exptid directory after completion of test ---------"
echo " "
echo " or you could run tail -f run_$exptid.log in a different window "
echo " "
echo " Please be patient, experiment could take about 5 minutes to complete"
echo " "
# save stdout and stderr to file descriptors 3 and 4, then redirect them to a file
exec 3>&1 4>&2 >run_$exptid.log 2>&1

sudo python dcell/dcell/dcellmain.py -B $bandwidth -n $numHosts -l 1 -d $directoryName --iperf $iperf

# restore stdout and stderr
exec 1>&3 2>&4

echo " ----- Experiment Done ------------------------------------"
echo " "
echo " ****** Creating output plots  ****************************"

sudo python dcell/dcell/util/plot_rate.py --rx --maxy $bandwidth --xlabel 'Time (seconds)' --ylabel \
                                          'Rate (Mbps)' -i '0_1_1-eth1' -f $directoryName/bwm.txt   \
                                            -o $directoryName/dcell_rate_0_1_1-eth1.png

sudo python dcell/dcell/util/plot_rate.py --rx --maxy $bandwidth --xlabel 'Time (seconds)' --ylabel \
                                          'Rate (Mbps)' -i '0_2_1-eth3' -f $directoryName/bwm.txt   \
                                            -o $directoryName/dcell_rate_0_2_1-eth3.png



