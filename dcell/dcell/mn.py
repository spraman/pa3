"""Custom topologies for Mininet

author: Brandon Heller (brandonh@stanford.edu)

To use this file to run a DCell-specific topology on Mininet.  Example:

  sudo mn --custom ~/PA3/dcell/dcell/mn.py --topo ft,4,1
"""

from dcell.dctopo import DCellTopo #FatTreeTopo, VL2Topo, TreeTopo

topos = { 'dc': DCellTopo}
#          'ft': FatTreeTopo,
#          'vl2': VL2Topo,
#          'tree': TreeTopo }
