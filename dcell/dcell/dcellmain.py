#!/usr/bin/python

"CS244 Assignment 3: DCell"

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.node import RemoteController
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg
from mininet.util import dumpNodeConnections, custom
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser
from util.monitor import monitor_devs_ng

from dcell.dctopo import DCellTopo
import shlex
import sys
import os

# Parse arguments
parser = ArgumentParser(description="Buffer sizing tests")
parser.add_argument('--bw', '-B',
                    dest="bw",
                    type=float,
                    action="store",
                    help="Bandwidth of host links",
                    required=True)

parser.add_argument('--dir', '-d',
                    dest="dir",
                    action="store",
                    help="Directory to store outputs",
                    default="results",
                    required=True)

parser.add_argument('-n',
                    dest="n",
                    type=int,
                    action="store",
                    help="Number of servers in a level",
                    required=True)

parser.add_argument('-l',
                    dest="l",
                    type=int,
                    action="store",
                    help="Number of levels",
                    required=True)

parser.add_argument('--iperf',
                    dest="iperf",
                    help="path to Custome iperf",
                    required=True)

# Expt parameters
args = parser.parse_args()

CUSTOM_IPERF_PATH = args.iperf
assert(os.path.exists(CUSTOM_IPERF_PATH))

if not os.path.exists(args.dir):
    os.makedirs(args.dir)

lg.setLogLevel('info')

def waitListening(client, server, port):
    "Wait until server is listening on port"
    if not 'telnet' in client.cmd('which telnet'):
        raise Exception('Could not find telnet')
    cmd = ('sh -c "echo A | telnet -e A %s %s"' %
           (server.IP(), port))
    while 'Connected' not in client.cmd(cmd):
        #output('waiting for', server, 'to listen on port', port, '\n')
        sleep(.5)

def start_receivers(net):
    # get Receiver and Sender 
    sender = net.getNodeByName('1_1_1')
    recvr = net.getNodeByName('1_5_4')
    # Start the Receiver
    port = 5001
    start_rcvr_cmd = ("%s -s -p %s > %s/iperf_server.txt &" % (CUSTOM_IPERF_PATH, port, args.dir))
    print "iperf (server/receiver) cmd: " + start_rcvr_cmd
    recvr.cmd(start_rcvr_cmd)
    waitListening(sender, recvr, port)

def start_senders(net):
    seconds = 200
    sender = net.getNodeByName('1_1_1')
    recvr = net.getNodeByName('1_5_4')
    port = 5001
    # iperf command based on time
    sender_cmd = ("%s -Z reno -c %s -p %d -t %d -i 1 -yc > %s/iperf_%s.txt" % (CUSTOM_IPERF_PATH, recvr.IP(), port, seconds, args.dir, sender))
    # iperf command based on number of bytes to be sent
    #sender_cmd = ("%s -Z reno -c %s -p %d -n 100M -i 1 -yc > %s/iperf_%s.txt" % (CUSTOM_IPERF_PATH, recvr.IP(), port, args.dir, sender))
    print "iperf (client/sender)   cmd: " + sender_cmd
    sender.sendCmd(sender_cmd)

def main():
    "Create network and run DCell Experiment"

    topo = DCellTopo(n=args.n, l=args.l)
    host = custom(CPULimitedHost, cpu=0.5)
    link = custom(TCLink, bw=args.bw)
    net  = Mininet(topo=topo, host=host, link=link, controller=RemoteController, autoSetMacs=True)

    net.start()

    raw_args = shlex.split("/home/ubuntu/pox/pox.py dcellpox.dcellpox --topo=dc,%s,1" % args.n)
    p = Popen(raw_args)
    print "-----------------------------------------------------------"
    print "-------------- Started DCellPox ---------------------------"
    print "-----------------------------------------------------------"
    sleep(10)

    #CLI(net)

    # Start the bandwidth monitor in the background
    monitor = Process(target=monitor_devs_ng, args=('%s/bwm.txt' % args.dir, 1.0))
    monitor.start()

    start_receivers(net)
    start_senders(net)
    start = time()
    print "Starting experiment"
    sleep(34)
    sender = net.getNodeByName('1_1_1')
    linkDownNode = net.getNodeByName('0_1_4')
    linkDownCmd = "ip link set 0_1_4-eth3 down"
    print linkDownCmd
    linkDownNode.cmd(linkDownCmd)
    print "pull link down"

    print("curr_time = %.3f seconds " % (time()-start))
    sleep(8)
    linkUpCmd = "ip link set 0_1_4-eth3 up"
    print linkUpCmd
    linkDownNode.cmd(linkUpCmd)
    print "restore link up"
    print("curr_time = %.3f seconds " % (time()-start))

    sleep(62)

    # server down is modelled by pulling down all the links attached to the server
    print "Server failure"
    print("curr_time = %.3f seconds " % (time()-start))
    linkDownCmd = "ip link set 0_1_4-eth1 down"
    print linkDownCmd
    linkDownNode.cmd(linkDownCmd)

    linkDownCmd = "ip link set 0_1_4-eth2 down"
    print linkDownCmd
    linkDownNode.cmd(linkDownCmd)

    linkDownCmd = "ip link set 0_1_4-eth3 down"
    print linkDownCmd
    linkDownNode.cmd(linkDownCmd)

    # Wait for iperf to finish
    sender.waitOutput()
    # Shut down iperf processes
    os.system('killall -9 ' + CUSTOM_IPERF_PATH)

    # Shut down monitors
    monitor.terminate()

    Popen("killall -9 top bwm-ng", shell=True).wait()

    net.stop()
    p.terminate()
    end = time()
    print("Experiment took %.3f seconds" % (end - start))

if __name__ == '__main__':
    main()
