#!/usr/bin/env python
'''@package dctopo

DCell Data center network topology creation and drawing.

@author Raman Subramanian, Jitendra Pandey

This package includes code to create and draw networks with a regular,
repeated structure.  The main class is StructuredTopo, which augments the
standard Mininet Topo object with layer metadata plus convenience functions to
enumerate up, down, and layer edges.
'''

from mininet.topo import Topo

class NodeID(object):
    '''Topo node identifier.'''
    def __init__(self, dpid = None):
        '''Init.
        @param dpid dpid
        '''
        # DPID-compatible hashable identifier: opaque 64-bit unsigned int
        self.dpid = dpid

    def __str__(self):
        '''String conversion.
        @return str dpid as string
        '''
        return str(self.dpid)

    def name_str(self):
        '''Name conversion.
        @return name name as string
        '''
        return str(self.dpid)

    def ip_str(self):
       '''Name conversion.
        @return ip ip as string
        '''
       hi  = (self.dpid & 0xff0000) >> 16
       mid = (self.dpid & 0xff00) >> 8
       lo  = self.dpid & 0xff
       return "10.%i.%i.%i" % (hi,mid,lo)

class DCellTopo(Topo):
    "DCell Topology"
    LAYER_SWITCH = 0
    LAYER_HOST = 1

    class DCellNodeID(NodeID):
        '''DCell-specific node.'''

        def __init__(self, isHostVar = 0, cellNum = 0, host = 0, dpid = None, name = None):
            if dpid:
                self.isHostVar = (dpid & 0xff0000) >> 16
                self.cellNum = (dpid & 0xff00) >> 8
                self.host = (dpid & 0xff)
                self.dpid = dpid
            elif name:
		isHostVar, cellNum, host = [int(s) for s in name.split('_')]
		self.isHostVar = isHostVar
                self.cellNum = cellNum
                self.host = host
                self.dpid = (isHostVar << 16) + (cellNum << 8) + host
            else:
		self.isHostVar = isHostVar
                self.cellNum = cellNum
                self.host = host
                self.dpid = (isHostVar << 16) + (cellNum << 8) + host

        def __str__(self):
            return "(%i, %i, %i)" % (self.isHostVar, self.cellNum, self.host)

        def name_str(self):
            '''Return name string'''
	    return "%i_%i_%i" % (self.isHostVar, self.cellNum, self.host)

        def mac_str(self):
            '''Return MAC string'''
            return "00:00:00:%02x:%02x:%02x" % (self.isHostVar, self.cellNum, self.host)

        def ip_str(self):
            '''Return IP string'''
            return "10.%i.%i.%i" % (self.isHostVar, self.cellNum, self.host)

	def get_id(self):
            '''Return dpid'''
	    return self.dpid

	def isHost(self):
            '''Return 1 if host'''
	    return self.isHostVar

    def def_nopts(self, layer, name = None):
        '''Return default dict for a FatTree topo.

        @param layer layer of node
        @param name name of node
        @return d dict with layer key/val pair, plus anything else (later)
        '''
        d = {'layer': layer}
        if name:
            id = self.id_gen(name = name)
            # For hosts only, set the IP
            if layer == self.LAYER_HOST:
              d.update({'ip': id.ip_str()})

            d.update({'mac': id.mac_str()})
            d.update({'dpid': "%016x" % id.dpid})
        return d


    def __init__(self, n=4, l=1, bw = 100, cpu=None):

        # Add default members to class.
        super(DCellTopo, self ).__init__()
        self.n = n
        self.l = l

        self.id_gen = DCellTopo.DCellNodeID
        print "DCellTopo: n =", n, "level=", l
        self.cpu = cpu
        self.bw = bw
        self.create_topology()

    def create_topology(self):
        #lconfig = { } 
        lconfig = {'bw' : self.bw } # 1000Mbps = 1Gbps
        # Make sure the bandwidth between the Host and switch of
        # the Host-switch-combo is never a bottleneck
        #lconfig1 = { } 
        lconfig1 = {'bw' : self.bw*10 } # 10000Mbps = 10Gbps
        for s in range(1, self.n+2):
	    # switch connecting hosts within a Dcell
            switch_mini = self.id_gen(self.LAYER_SWITCH, s, self.n+1).name_str()
            switch_opts = self.def_nopts(self.LAYER_SWITCH, switch_mini)
            switch_mini = self.add_switch(switch_mini, **switch_opts)
	    for h in range(1, self.n+1):
		# create a Host-Switch combo
                host_curr   = self.id_gen(self.LAYER_HOST, s, h).name_str()
                host_opts   = self.def_nopts(self.LAYER_HOST, host_curr)
                host_curr   = self.add_host(host_curr, **host_opts)

                switch_curr   = self.id_gen(self.LAYER_SWITCH, s, h).name_str()
                switch_opts   = self.def_nopts(self.LAYER_SWITCH, switch_curr)
                switch_curr   = self.add_switch(switch_curr, **switch_opts)

                self.add_link(switch_curr, host_curr, port1=1, port2=0, **lconfig1)   
                self.add_link(switch_mini, switch_curr, port1=h, port2=2, **lconfig)   

        for i in range(1, self.n+2):
            for j in range(i+1, self.n+2):
		# link up the switches of the Host-switch combo from different Dcells
                uid_1, uid_2 = j-1, i
                host_1 = self.id_gen(self.LAYER_SWITCH, i, uid_1).name_str()
                host_2 = self.id_gen(self.LAYER_SWITCH, j, uid_2).name_str()
                self.add_link(host_1, host_2, port1=3, port2=3, **lconfig)   
                #print "host_1:" + host_1 + "host_2:" + host_2

    def getNodeName(self, hs, c, h):
	return str(hs) + "_" + str(c) + "_" + str(h)

    def getTheMiniSwitch(self, cell) :
	miniSwitchname = self.getNodeName(0, cell, 5)
	return miniSwitchname

    def correspondingCellSwitches(self, cell1, cell2):
        if (cell1 == cell2):
	    return None
	if (cell1 < cell2) :
	    src = self.getNodeName(0, cell1, cell2-1)
	    dst = self.getNodeName(0, cell2, cell1)
	else:
            src = self.getNodeName(0, cell1, cell2)
	    dst = self.getNodeName(0, cell2, cell1-1)
        return (src, dst)

    def get_dcell_route(self, src, dst) :
        #print "DEBUG src = " + src + ", dst = " + dst
	srcId = self.id_gen(name = src)
	dstId = self.id_gen(name = dst)
	if (srcId.get_id() == dstId.get_id()):
	    route = [src]
	elif (srcId.cellNum == dstId.cellNum):
	    msw = self.getTheMiniSwitch(srcId.cellNum)
            #print "DEBUG 0.0 src = " + src + ", dst = " + dst
	    route = [src, msw, dst]
	else:
	    srcCellSwitch, dstCellSwitch = self.correspondingCellSwitches(srcId.cellNum, dstId.cellNum)
	    if (srcCellSwitch == src):
		if (dstCellSwitch == dst):
		    route = [src, dst]
		else:
		    remoteMiniSw = self.getTheMiniSwitch(dstId.cellNum)
		    route = [src, dstCellSwitch, remoteMiniSw, dst]
	    else :
                if (dstCellSwitch == dst):
		    route = [src, self.getTheMiniSwitch(srcId.cellNum), srcCellSwitch, dst]
		else :
		    route = [src, self.getTheMiniSwitch(srcId.cellNum), srcCellSwitch, 
			    dstCellSwitch, 
			    self.getTheMiniSwitch(dstId.cellNum), 
			    dst] 
        if len(route)>1 and route[0] == route[1] :
	    route.pop(0)
	return route


    #Returns a switch different from the given node in the same cell
    def get_another_switch(self, node):
	nodeId = self.id_gen(name = node)
	return self.getNodeName(0, nodeId.cellNum, (nodeId.host % 4)+1)

    def get_remote_switch(self, node) :
        nodeId = self.id_gen(name = node)
	if (nodeId.host < nodeId.cellNum):
           return self.getNodeName(0, nodeId.host, nodeId.cellNum-1) 
        else:
	   return self.getNodeName(0, nodeId.host+1, nodeId.cellNum)
   
    def get_failover_route(self, src, dst, failedLink) :
        #First get the normal route
	normalRoute = self.get_dcell_route(src, dst)
	if (not failedLink) or (len(failedLink) < 2):
	    return normalRoute
	failedLinkSwitch1InRoute = False
	failedLinkSwitch2InRoute = False
        for node in normalRoute:
	    if (node == failedLink[0]):
		failedLinkSwitch1InRoute = True
	    if (node == failedLink[1]) :
		failedLinkSwitch2InRoute = True
	if (failedLinkSwitch1InRoute and failedLinkSwitch2InRoute) :
	    reroutedPath = []
	    if (src == failedLink[0] or src == failedLink[1]) :
		localProxy = self.get_another_switch(src)
                newPath = self.get_dcell_route(self, src, localProxy)
		pathFromLocalProxy = self.get_local_reroute_path(self,
			localProxy, dst)
		for node in pathFromLocalProxy:
		    if (node != localProxy) :
		       newPath.append(node)
                return newPath
	    else :
		return self.get_local_reroute_path(src, dst)
	else :
	    return normalRoute
            

    #Hop to a remote dcell then route normally. 
    #Assumes that there is no failed link in 
    #re-routed path.
    def get_local_reroute_path(self, src, dst) :
        remoteSw = self.get_remote_switch(src)
	path = [src]
	pathFromRemoteSw = self.get_dcell_route(remoteSw, dst)
	for node in pathFromRemoteSw:
	    path.append(node)
	return path


